package com.phicorb.eventwatchface;

import android.content.BroadcastReceiver;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Message;
import android.os.PowerManager;
import android.support.wearable.provider.WearableCalendarContract;
import android.support.wearable.watchface.CanvasWatchFaceService;
import android.support.wearable.watchface.WatchFaceStyle;
import android.text.DynamicLayout;
import android.text.Editable;
import android.text.SpannableStringBuilder;
import android.text.TextPaint;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.SurfaceHolder;

public class EventWatchFace extends CanvasWatchFaceService
{
    private static final String TAG = "EventWatchFace";

    @Override
    public Engine onCreateEngine()
    {
        return new Engine();
    }

    private class Engine extends CanvasWatchFaceService.Engine
    {
        static final int BACKGROUND_COLOR = Color.BLACK;
        static final int FOREGROUND_COLOR = Color.WHITE;
        static final int TEXT_SIZE = 25;
        static final int MSG_LOAD_MEETINGS = 0;

        /** Editable string containing the text to draw with the number of meetings in bold. */
        final Editable mEditable = new SpannableStringBuilder();

        /** Width specified when {@link #mLayout} was created. */
        int mLayoutWidth;

        /** Layout to wrap {@link #mEditable} onto multiple lines. */
        DynamicLayout mLayout;

        /** Paint used to draw text. */
        final TextPaint mTextPaint = new TextPaint();

        Paint pTextPaint = new Paint();
        String sText;
        float centerX, centerY;

        int mNumMeetings, nMin, nHr;

        private AsyncTask<Void, Void, Integer> mLoadMeetingsTask;

        /** Handler to load the meetings once a minute in interactive mode. */
        final Handler mLoadMeetingsHandler = new Handler()
        {
            @Override
            public void handleMessage(Message message)
            {
                switch (message.what)
                {
                    case MSG_LOAD_MEETINGS:
                        cancelLoadMeetingTask();
                        mLoadMeetingsTask = new LoadMeetingsTask();
                        mLoadMeetingsTask.execute();
                        break;
                }
            }
        };

        private boolean mIsReceiverRegistered;

        private BroadcastReceiver mBroadcastReceiver = new BroadcastReceiver()
        {
            @Override
            public void onReceive(Context context, Intent intent)
            {
                if (Intent.ACTION_PROVIDER_CHANGED.equals(intent.getAction())
                        && WearableCalendarContract.CONTENT_URI.equals(intent.getData()))
                {
                    cancelLoadMeetingTask();
                    mLoadMeetingsHandler.sendEmptyMessage(MSG_LOAD_MEETINGS);
                }
            }
        };

        @Override
        public void onCreate(SurfaceHolder holder)
        {
            if (Log.isLoggable(TAG, Log.DEBUG))
            {
                Log.d(TAG, "onCreate");
            }
            super.onCreate(holder);
            setWatchFaceStyle(new WatchFaceStyle.Builder(EventWatchFace.this)
                    .setCardPeekMode(WatchFaceStyle.PEEK_MODE_VARIABLE)
                    .setBackgroundVisibility(WatchFaceStyle.BACKGROUND_VISIBILITY_INTERRUPTIVE)
                    .setShowSystemUiTime(true)
                    .setStatusBarGravity(Gravity.BOTTOM)
                    .setHotwordIndicatorGravity(Gravity.BOTTOM)
                    .build());

            mTextPaint.setColor(FOREGROUND_COLOR);
            mTextPaint.setTextSize(TEXT_SIZE);
            pTextPaint.setColor(FOREGROUND_COLOR);
            pTextPaint.setTextSize(TEXT_SIZE);
            pTextPaint.setTextAlign(Paint.Align.CENTER);
            pTextPaint.setAntiAlias(true);

            mLoadMeetingsHandler.sendEmptyMessage(MSG_LOAD_MEETINGS);
        }

        @Override
        public void onDestroy()
        {
            mLoadMeetingsHandler.removeMessages(MSG_LOAD_MEETINGS);
            cancelLoadMeetingTask();
            super.onDestroy();
        }

        @Override
        public void onDraw(Canvas canvas, Rect bounds)
        {
            // Find the center. Ignore the window insets so that, on round watches
            // with a "chin", the watch face is centered on the entire screen, not
            // just the usable portion.
            centerX = bounds.width() / 2f;
            centerY = bounds.height() / 2f;

            canvas.drawColor(BACKGROUND_COLOR);

            if (mNumMeetings < 1)
            {
                nMin = mNumMeetings % 60;
                nHr = (int)Math.floor(mNumMeetings / 60);
                pTextPaint.setFakeBoldText(true);
                centerY -= pTextPaint.getFontSpacing();
                sText = "No events";
                canvas.drawText(sText, centerX, centerY, pTextPaint);

                pTextPaint.setFakeBoldText(false);
                centerY = bounds.height() / 2f;
                sText = "in the next 24 hours";
                canvas.drawText(sText, centerX, centerY, pTextPaint);
            }
            else
            {
                if (mNumMeetings > 60)
                {
                    nMin = mNumMeetings % 60;
                    nHr = (int)Math.floor(mNumMeetings / 60);
                    pTextPaint.setFakeBoldText(true);
                    centerY -= pTextPaint.getFontSpacing();
                    sText = String.format("%d:%d", nHr, nMin);
                    canvas.drawText(sText, centerX, centerY, pTextPaint);

                    pTextPaint.setFakeBoldText(false);
                    centerY = bounds.height() / 2f;
                    sText = "until next event";
                    canvas.drawText(sText, centerX, centerY, pTextPaint);
                }
                else
                {
                    pTextPaint.setFakeBoldText(true);
                    centerY -= pTextPaint.getFontSpacing();
                    sText = String.format("%d minutes", mNumMeetings);
                    canvas.drawText(sText, centerX, centerY, pTextPaint);

                    pTextPaint.setFakeBoldText(false);
                    centerY = bounds.height() / 2f;
                    sText = "until next event";
                    canvas.drawText(sText, centerX, centerY, pTextPaint);
                }
            }
        }

        @Override
        public void onVisibilityChanged(boolean visible)
        {
            super.onVisibilityChanged(visible);
            if (visible)
            {
                IntentFilter filter = new IntentFilter(Intent.ACTION_PROVIDER_CHANGED);
                filter.addDataScheme("content");
                filter.addDataAuthority(WearableCalendarContract.AUTHORITY, null);
                registerReceiver(mBroadcastReceiver, filter);
                mIsReceiverRegistered = true;

                mLoadMeetingsHandler.sendEmptyMessage(MSG_LOAD_MEETINGS);
            }
            else
            {
                if (mIsReceiverRegistered)
                {
                    unregisterReceiver(mBroadcastReceiver);
                    mIsReceiverRegistered = false;
                }
                mLoadMeetingsHandler.removeMessages(MSG_LOAD_MEETINGS);
            }
        }

        private void onMeetingsLoaded(Integer result)
        {
            if (result != null)
            {
                mNumMeetings = result;
                invalidate();
            }
        }

        private void cancelLoadMeetingTask()
        {
            if (mLoadMeetingsTask != null)
            {
                mLoadMeetingsTask.cancel(true);
            }
        }

        /**
         * Asynchronous task to load the meetings from the content provider and report the number of
         * meetings back via {@link #onMeetingsLoaded}.
         */
        private class LoadMeetingsTask extends AsyncTask<Void, Void, Integer>
        {
            private PowerManager.WakeLock mWakeLock;

            @Override
            protected Integer doInBackground(Void... voids)
            {
                PowerManager powerManager = (PowerManager) getSystemService(POWER_SERVICE);
                mWakeLock = powerManager.newWakeLock(
                        PowerManager.PARTIAL_WAKE_LOCK, "EventWatchFaceWakeLock");
                mWakeLock.acquire();

                long begin = System.currentTimeMillis();
                Uri.Builder builder =
                        WearableCalendarContract.Instances.CONTENT_URI.buildUpon();
                ContentUris.appendId(builder, begin);
                ContentUris.appendId(builder, begin + DateUtils.DAY_IN_MILLIS);
                final Cursor cursor = getContentResolver().query(builder.build(),
                        null, null, null, null);
                int nMeetingColPos = cursor.getColumnIndex("DTSTART");
                cursor.moveToLast();
                long numMeetings = cursor.getLong(nMeetingColPos);
                long lMillTimeDiff = numMeetings - begin;
                int nMinTimeDiff = (int)lMillTimeDiff / 60000;
                if (Log.isLoggable(TAG, Log.VERBOSE))
                {
                    Log.v(TAG, "Minutes to next meeting: " + numMeetings);
                }
                cursor.close();
                return nMinTimeDiff;
            }

            @Override
            protected void onPostExecute(Integer result)
            {
                releaseWakeLock();
                onMeetingsLoaded(result);
            }

            @Override
            protected void onCancelled()
            {
                releaseWakeLock();
            }

            private void releaseWakeLock()
            {
                if (mWakeLock != null)
                {
                    mWakeLock.release();
                    mWakeLock = null;
                }
            }
        }
    }
}
